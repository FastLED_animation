#include "FastLED_animation.h"

Animation::Animation() {
	init(0, NULL, 0);
}

Animation::Animation(uint16_t frameCount_,
		     const prog_uint8_t* frameData_,
		     const uint8_t ledCount_)
{
	init(frameCount_, frameData_, ledCount_);
	reset();
}

void Animation::init(uint16_t frameCount_,
		     const prog_uint8_t* frameData_,
		     const uint8_t ledCount_)
{
	frameCount = frameCount_;
	frameData = const_cast<prog_uint8_t*>(frameData_);
	ledCount = ledCount_;

	// Load the color table into memory
	// TODO: Free this memory somewhere?
	colorTableEntries = pgm_read_byte(frameData) + 1;
	colorTable = (CRGB *)malloc(colorTableEntries * sizeof(CRGB));

	for(int i = 0; i < colorTableEntries; i++) {
		colorTable[i] = CRGB(pgm_read_byte(frameData + 1 + i * 3    ),
				     pgm_read_byte(frameData + 1 + i * 3 + 1),
				     pgm_read_byte(frameData + 1 + i * 3 + 2));
	}

	reset();
}

void Animation::reset() {
	frameIndex = 0;
}

void Animation::draw(struct CRGB strip[]) {
	drawIndexed_RLE(strip);
	LEDS.show();

	frameIndex = (frameIndex + 1)%frameCount;
};

void Animation::drawIndexed_RLE(struct CRGB strip[]) {
	if(frameIndex == 0) {
		currentFrameData = frameData
			+ 1 + 3 * colorTableEntries;   // Offset for color table
	}

	// Read runs of RLE data until we get enough data.
	uint8_t count = 0;
	while (count < ledCount) {
		uint8_t run_length = pgm_read_byte(currentFrameData++);
		uint8_t colorIndex = pgm_read_byte(currentFrameData++);

		for(uint8_t i = 0; i < run_length; i++) {
			strip[count++] = colorTable[colorIndex];
		}
	}
};
